spymemcached (2.12.3+dfsg-3+apertis1) apertis; urgency=medium

  * Move package to development repository. Needed for the Java suite

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Tue, 10 Oct 2023 22:23:52 +0530

spymemcached (2.12.3+dfsg-3+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 05 Oct 2023 17:45:25 +0000

spymemcached (2.12.3+dfsg-3) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Apply multi-arch hints. + libspymemcached-java: Add Multi-Arch: foreign.

  [ Jochen Sprickerhof ]
  * Add patch to fix FTBFS (Closes: #1012108)
  * Add missing build dependency on log4j 1.2

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 26 Feb 2023 17:08:21 +0100

spymemcached (2.12.3+dfsg-2+apertis6) apertis; urgency=medium

  * Enable testLibKetamaWeightedCompatLibmemcached test (DBTS: #990278)

 -- Vignesh Raman <vignesh.raman@collabora.com>  Mon, 04 Jul 2022 12:05:33 +0530

spymemcached (2.12.3+dfsg-2+apertis5) apertis; urgency=medium

  * Set component to sdk. Move java packages to sdk to avoid building
    for arm architecture.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Mon, 28 Feb 2022 20:29:11 +0530

spymemcached (2.12.3+dfsg-2+apertis4) apertis; urgency=medium

  * Set component to development. This is done to split the disable
    test fix and setting component to sdk in two seperate commits,
    so that the disable test fix can be backported to older releases.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Mon, 28 Feb 2022 19:09:13 +0530

spymemcached (2.12.3+dfsg-2+apertis3) apertis; urgency=medium

  * Set component to sdk. Move java packages to sdk to avoid building
    for arm architecture.
  * Remove testLibKetamaWeightedCompatLibmemcached test again, as OBS
    build is failing on this due to hostname different than 'localhost'.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Tue, 22 Feb 2022 17:22:39 +0530

spymemcached (2.12.3+dfsg-2+apertis2) apertis; urgency=medium

  * Drop patch that disabled testLibKetamaWeightedCompatLibmemcached test.
    OBS has now been fixed to match a reverse lookup on loopback address
    127.0.0.1 to 'localhost'.

 -- Ariel D'Alessandro <ariel.dalessandro@collabora.com>  Wed, 11 Aug 2021 15:27:45 -0300

spymemcached (2.12.3+dfsg-2apertis1) apertis; urgency=medium

  * d/patches: Remove testLibKetamaWeightedCompatLibmemcached test, as OBS
    build is failing on this due to hostname different than 'localhost'.

 -- Ariel D'Alessandro <ariel.dalessandro@collabora.com>  Tue, 27 Apr 2021 12:50:07 -0300

spymemcached (2.12.3+dfsg-2apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 21 Apr 2021 11:12:03 +0200

spymemcached (2.12.3+dfsg-2) unstable; urgency=medium

  * Switching to debhelper-compat to denote debhelper compatibility level.

 -- Andrius Merkys <merkys@debian.org>  Mon, 11 Nov 2019 07:37:32 -0500

spymemcached (2.12.3+dfsg-1) unstable; urgency=medium

  [ Christopher Hoskin ]
  * Initial release (Closes: #845789)

 -- Andrius Merkys <merkys@debian.org>  Tue, 08 Oct 2019 06:51:45 -0400
